# wallsplat! Prototype

This is a work-in-progress prototype of a game concept I've had in mind for a few years, under the working title _wallsplat!_

This prototype is developed as part of the [2024 Spring Lisp Game Jam](https://itch.io/jam/spring-lisp-game-jam-2024) in the [Fennel](https://fennel-lang.org/) programming language.

## Dependencies

- just task runner
- Fennel
- LOVE2D game runtime

## Building

- `just build` to build
- `just run` to build and start in LOVE
- `just clean` to clean lua build artifacts
