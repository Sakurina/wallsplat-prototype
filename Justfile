# Compile Fennel code to Lua
build:
	fennel --compile conf.fnl > conf.lua
	fennel --compile main.fnl > main.lua

# Compile Fennel code to Lua and run in LOVE2D
run:
	fennel --compile conf.fnl > conf.lua
	fennel --compile main.fnl > main.lua
	love .

# Clean built Fennel code
clean:
	rm conf.lua main.lua
