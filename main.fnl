(local root-state { 
  :screen :controls
  :width 320   ; 16:9
  ;width 240    ; 4:3
  :height 180
  :int-scale 4
  :transition-target 0.2
  :logging-enabled true
  :controller-handle nil
  :dpad-up nil
  :dpad-down nil
  :a-button nil
  :start-button nil
})

(local controls-state {
  :current-transition :none 
  :transition-timer 0.0 
})

(local splash-state { 
  :current-transition :none 
  :transition-timer 0.0 
  :selected-item :start
})

(local default-game-state {
  :paused false
  :game-over false
  :chara-x 158
  :chara-y 88
  :chara-snapping false
  :chara-snap-x 158
  :chara-snap-y 88
  :chara-snap-delay 0.1
  :chara-snap-counter 0
  :analog-x 0
  :analog-y 0
  :timer 30
})

(local lume (require :lume))
(local lovebird (require :lovebird))
(local font (love.graphics.newFont "PixeloidSans.otf" 9))

(var game-state (lume.clone default-game-state))

(fn ctrl-pressed [button]
  (root-state.controller-handle.isGamepadDown root-state.controller-handle button))

(fn log [msg]
  (when (. root-state :logging-enabled) (lovebird.print msg)))

(fn scale [int-scale]
  (set root-state.int-scale int-scale)
  (love.window.setMode (* int-scale root-state.width) (* int-scale root-state.height)))

;; TRANSITION BAR
;;================

(fn draw-transition-bar [state]
  (love.graphics.setColor 1 1 1 1)
  (local bar-height 3)
  (local bar-width (* root-state.width (/ state.transition-timer root-state.transition-target)))
  (local bar-x (- (/ root-state.width 2) (/ bar-width 2)))
  (local bar-y (- root-state.height bar-height))
  (love.graphics.rectangle "fill" bar-x bar-y bar-width bar-height))

(fn update-transition-bar [dt state]
  (when (not= state.current-transition :none) 
    (set state.transition-timer (+ state.transition-timer dt))
    (when (> state.transition-timer root-state.transition-target)
      (set root-state.screen state.current-transition)
      (set state.current-transition :none)
      (set state.transition-timer 0.0))))

;; CONTROLS SCREEN
;;=================


(fn controls-update [dt]
  (update-transition-bar dt controls-state))

(fn controls-draw []
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.rectangle "fill" 0 0 root-state.width root-state.height)

  (love.graphics.setColor 1 1 1 1)
  (love.graphics.printf "This game requires a controller with an analog stick to play." 0 74 root-state.width "center")
  (love.graphics.setColor 0.6 0.6 0.6 1)
  (love.graphics.printf "Press Start on the controller you wish to use." 0 85 root-state.width "center")
  (love.graphics.printf "Press Q to quit." 0 96 root-state.width "center")
  (draw-transition-bar controls-state))

(fn controls-keys [key]
  (case key
    "q" (love.event.quit)))

(fn extract-button-index [joystick button-name]
  (let [(_ idx _) (joystick.getGamepadMapping joystick button-name)] idx))

(fn controls-joystick [joystick button]
  (when (and (= root-state.controller-handle nil) (joystick.isGamepadDown joystick "start"))
    (set root-state.controller-handle joystick)
    (set root-state.dpad-up (extract-button-index joystick "dpup"))
    (set root-state.dpad-down (extract-button-index joystick "dpdown"))
    (set root-state.a-button (extract-button-index joystick "a"))
    (set root-state.start-button (extract-button-index joystick "start"))
    (set controls-state.current-transition :splash)))

;; SPLASH SCREEN
;;===============


(fn scroll-up []
  (case splash-state.selected-item
    :start (set splash-state.selected-item :quit)
    :controls (set splash-state.selected-item :start)
    :quit (set splash-state.selected-item :controls)))

(fn scroll-down []
  (case splash-state.selected-item
    :start (set splash-state.selected-item :controls)
    :controls (set splash-state.selected-item :quit)
    :quit (set splash-state.selected-item :start)))

(fn start-game []
  (set game-state (lume.clone default-game-state))
  (set splash-state.current-transition :game))

(fn go []
  (case splash-state.selected-item
    :start (start-game)
    :controls (set splash-state.current-transition :controls)
    :quit (love.event.quit)))

(fn splash-update [dt]
  (update-transition-bar dt splash-state))

(fn set-menu-item-color [item]
  (when (not= splash-state.selected-item item) (love.graphics.setColor 1 1 1 1))
  (when (= splash-state.selected-item item) (love.graphics.setColor 1 0 0 1)))

(fn splash-draw []
  ; bg
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.rectangle "fill" 0 0 root-state.width root-state.height)

  ; menu options
  (set-menu-item-color :start)
  (love.graphics.print "Start" 12 129)
  (set-menu-item-color :controls)
  (love.graphics.print "Controls" 18 144)
  (set-menu-item-color :quit)
  (love.graphics.print "Quit" 24 159)
  (draw-transition-bar splash-state))

(fn splash-digital [joystick button]
  (local dpup root-state.dpad-up)
  (local dpdown root-state.dpad-down)
  (local a root-state.a-button)
  (when (= button dpup) (scroll-up))
  (when (= button dpdown) (scroll-down))
  (when (= button a) (go)))

;; MAIN GAME
;;===========

(fn snap-velocity [before-x before-y after-x after-y]
  ; calculate pull distance in pixels
  (local distance (lume.distance before-x before-y after-x after-y))
  ; set other values
  (local mass 1) ; grams
  (local spring-constant 17.38) ; N/m
  ; v = sqrt(k/m) * x
  ; where:
  ; v = initial velocity
  ; k = spring constant
  ; m = mass of rubber band
  ; x = distance pulled
  (* (math.sqrt (/ spring-constant mass)) distance)
)

(fn player-did-release? [old-rel-x old-rel-y new-rel-x new-rel-y]
  (local new-in-bounds (and (> new-rel-x -0.1) (< new-rel-x 0.1) (> new-rel-y -0.1) (< new-rel-y 0.1)))
  (local old-in-bounds (or (< old-rel-x -0.1) (> old-rel-x 0.1) (< old-rel-y -0.1) (> old-rel-y 0.1)))
  (and new-in-bounds old-in-bounds)
)

(fn player-released [old-rel-x old-rel-y new-rel-x new-rel-y]
  (log "player released")
  ; calculate velocity
  (local velocity (snap-velocity old-rel-x old-rel-y new-rel-x new-rel-y))
  ; calculate destination
  (local arrow-x (* old-rel-x 12))
  (local arrow-y (* old-rel-y 12))
  ; map-old-rel in square-space
  (local dest-x (* -1 (* arrow-x velocity)))
  (local dest-y (* -1 (* arrow-y velocity)))
  (local dest-abs-x (+ game-state.chara-snap-x dest-x))
  (local dest-abs-y (+ game-state.chara-snap-y dest-y))
  (set game-state.chara-snapping true)
  (set game-state.chara-snap-x dest-abs-x)
  (set game-state.chara-snap-y dest-abs-y)
  (set game-state.chara-snap-counter 0))

(fn game-analog []
  (local joystick root-state.controller-handle)
  (local old-x game-state.analog-x)
  (local old-y game-state.analog-y)
  (set game-state.analog-x (joystick.getAxis joystick 1))
  (set game-state.analog-y (joystick.getAxis joystick 2))
  (when (player-did-release? old-x old-y game-state.analog-x game-state.analog-y) (player-released old-x old-y game-state.analog-x game-state.analog-y)))

(fn perform-snap-positioning [dt]
  (when game-state.chara-snapping
    (log "snap positioning")
    (set game-state.chara-snap-counter (+ game-state.chara-snap-counter dt))
    (local interp (lume.clamp (/ game-state.chara-snap-counter game-state.chara-snap-delay) 0 1))
    (local abs-x (lume.smooth game-state.chara-x game-state.chara-snap-x interp))
    (local abs-y (lume.smooth game-state.chara-y game-state.chara-snap-y interp))
    (set game-state.chara-x abs-x)
    (set game-state.chara-y abs-y)
    (when (= interp 1) (set game-state.chara-snapping false))
  )
)

(fn game-unpaused-update [dt]
  (perform-snap-positioning dt)
  (set game-state.timer (lume.clamp (- game-state.timer dt) 0 30))
  (when (= game-state.timer 0) (set game-state.game-over true))
  (game-analog))

(fn game-paused-update [dt])

(fn game-update [dt]
  (when (. game-state :paused) (game-paused-update dt))
  (when (not (. game-state :paused)) (game-unpaused-update dt)))

(fn game-draw-bg []
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.rectangle "fill" 0 0 root-state.width root-state.height))

(fn game-draw-character []
  (love.graphics.setColor 0 1 0 1)
  (local chara-width 4)
  (local chara-height 4)
  (local x (- game-state.chara-x (/ chara-width 2)))
  (local y (- game-state.chara-y (/ chara-height 2)))
  (love.graphics.rectangle "line" x y chara-width chara-height))

(fn game-draw-arrow []
  (when (or (not= game-state.analog-x 0) (not= game-state.analog-y 0))
    ; draw middle line
    (love.graphics.setColor 1 0 0 1)
    (local max-arrow 12)
    (local arrow-x (+ game-state.chara-x (* game-state.analog-x max-arrow)))
    (local arrow-y (+ game-state.chara-y (* game-state.analog-y max-arrow)))
    (love.graphics.line game-state.chara-x game-state.chara-y arrow-x arrow-y)))

(fn game-draw-timer []
  (love.graphics.setColor 1 0 0 1)
  (local bar-height 3)
  (local bar-width (* root-state.width (/ game-state.timer 30)))
  (local bar-x (- (/ root-state.width 2) (/ bar-width 2)))
  (local bar-y 0)
  (love.graphics.rectangle "fill" bar-x bar-y bar-width bar-height)
  (local timer-txt (lume.round game-state.timer 0.01))
  (love.graphics.print timer-txt 2 5))

(fn game-draw-paused []
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.print "Paused" 2 (- root-state.height 14)))

(fn game-draw-game-over []
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.print "Game Over" 2 (- root-state.height 14)))

(fn game-draw []
  (game-draw-bg)
  (game-draw-character)
  (game-draw-timer)
  (game-draw-arrow)
  (when game-state.paused (game-draw-paused))
  (when game-state.game-over (game-draw-game-over)))

(fn game-keys [key]
  (case key
    "q" (set root-state.screen :splash)))

(fn game-digital [joystick button]
  (local start root-state.start-button)
  (when (= button start)
    (when (not game-state.game-over) (set game-state.paused (not game-state.paused)))
    (when game-state.game-over (set root-state.screen :splash))))

;; LOVE CALLBACKS
;;================

(local screen-map {
  :controls {
    :update controls-update
    :draw controls-draw
    :keypressed controls-keys
    :joystickpressed controls-joystick
  }
  :splash {
    :update splash-update
    :draw splash-draw
    :joystickpressed splash-digital
  }
  :game {
    :update game-update
    :draw game-draw
    :keypressed game-keys
    :joystickpressed game-digital
  }
})

(fn love.load [] 
  (love.window.setTitle "wallsplat! (Prototype)")
  (love.graphics.setDefaultFilter "nearest" "nearest")
  (font.setFilter font "nearest" "nearest")
  (scale root-state.int-scale)
  (love.graphics.setFont font))

(fn love.update [dt]
  (when (. root-state :logging-enabled) (lovebird.update))
  ((. (. screen-map root-state.screen) :update) dt))

(fn love.draw []
  (love.graphics.push)
  (love.graphics.scale root-state.int-scale root-state.int-scale)
  ((. (. screen-map root-state.screen) :draw))
  (love.graphics.pop))

(fn love.keypressed [key]
  (set keypressed (?. (. screen-map root-state.screen) :keypressed))
  (when (not= keypressed nil) (keypressed key button)))

(fn love.joystickpressed [joystick button]
  (log button)
  (set joystickpressed (?. (. screen-map root-state.screen) :joystickpressed))
  (when (not= joystickpressed nil) (joystickpressed joystick button)))
